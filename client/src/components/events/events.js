import React from "react";
import "./events.css";
import numone from "../../assets/images/numone.svg";
import numtwo from "../../assets/images/numtwo.svg";
import numthree from "../../assets/images/numthree.svg";

export default function Events() {
  return (
    <>
      <div className="events">
        <div className="heading">
          <h1 className="head">Events</h1>
          <br />
          <p className="text">
            Hello fgyuejhruijeh werhyrtijehajshf eghreuegyhurh gfutfyserughfuij
            sfseueytrfuehfs fuefugsdhfhgsssf Hello fgyuejhruijeh
            werhyrtijehajshf eghreuegyhurh gfutfyserughfuij sfseueytrfuehfs
            fuefugsdhfhgsssf Hello fgyuejhruijeh werhyrtijehajshf eghreuegyhurh
            gfutfyserughfuij sfseueytrfuehfs fuefugsdhfhgsssf
          </p>
          <br />
        </div>
        <div className="events-card-wrapper">
          {" "}
          <div className="card11">
            <img className="imgone1" src={numone} alt="one" />
            <br />
            <p4>
              Efficacious <br /> Coding Challenge
            </p4>
            <br />
            <br />
            <p3>
              Participants will be provided
              <br /> some traditional
              <br /> programming questions/
              <br />
              algorithms to code in the
              <br /> most efficient way and in least
              <br /> lines.This will be a round-
              <br />
              based with contestants being
              <br /> removed after each round.
            </p3>
            <br />
            <br />
            <br />
            <br />
            <br />
            <a className="readmore" href="google.com">
              Read More
            </a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
          <div className="card22">
            <img className="imgone1" src={numtwo} alt="two" />
            <br />
            <p4>BattleCode</p4>
            <br />
            <br />
            <br />
            <p3>
              An online tech event in which
              <br /> Participants will have one
              <br /> hour to solve their assigned
              <br /> code. If they are successful,
              <br /> they will begin to search for
              <br /> other members of their squad
              <br /> who have the same output or
              <br /> one that is related to it.
            </p3>
            <br />
            <br />
            <br />
            <br />
            <br />
            <a className="readmore" href="google.com">
              Read More
            </a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
          <div className="card33">
            <img className="imgone1" src={numthree} alt="three" />
            <br />
            <p4>
              Shuffle
              <br /> Definace
            </p4>
            <br />
            <br />
            <p3>
              An online tech event in which
              <br /> the participants/teams will be
              <br /> handed codes that have
              <br /> some faults in them. We'll be
              <br /> providing the first code's
              <br /> input. They must debug the
              <br /> code, and the code's result
              <br /> will be used as the input for
              <br /> the following code.
            </p3>
            <br />
            <br />
            <br />
            <br />
            <a className="readmore" href="google.com">
              Read More
            </a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
    </>
  );
}
