import "./Footer.css";
import CTA from "../../assets/images/CTA .png";
import mono from "../../assets/images/monochrome-logo.png";
export default function Footer() {
  return (
    <>
      <footer>
        <div id="left-footer-wrapper">
          <img src={mono} alt="UCD-logo" />
          <div className="social">
            <a href="https://www.instagram.com/ucd.upes/?hl=en">
              {" "}
              <i className="fab fa-instagram "></i>
            </a>
            <a href="https://www.linkedin.com/in/upesucd/">
              {" "}
              <i className="fab fa-linkedin-in  "></i>{" "}
            </a>
            <a href="https://twitter.com/ucd_upes">
              {" "}
              <i className="fab fa-twitter"></i>
            </a>
            <a href="https://www.youtube.com/channel/UC6ix9dmrB-gaA1ERxNoSYwQ">
              {" "}
              <i className="fab fa-youtube"></i>
            </a>
          </div>
        </div>
        <div id="right-footer-wrapper">
          <div id="footer-nav">
            <ul>
              <li>
                <a href="https://ucdupes.org/Aboutus.html"> About</a>
              </li>
              <li>
                <a href="https://ucdupes.org/">Privacy </a>
              </li>
              <li>
                <a href="https://linktr.ee/UnitedClubOfDevelopers">
                  <img src={CTA} alt="join discord" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </>
  );
}
