import "./Hero.css";
import banner from "../../assets/images/banner.svg";

export default function Hero() {
  return (
    <>
      <h1>
        Welcome Onboard <br /> To The Second Edition of Orcas
      </h1>
      <img src={banner} alt="banner" />
      <p className="credits">Chill Mario BY PIXEL JEFF</p>
      <div className="hero-description">
        The second edition of our winter convention “Orcas” is live now. Orcas
        celebrate technologists, learners, and enthusiasts, and everyone else
        who shares a passion for technology. It will be a week packed with
        events and webinars for all the participants.
        <br />
        <br />
        <div className="buttons">
          <div className="divbutton">
            <button>Register Now</button>
          </div>
          <div className="divbutton">
            <button className="Knowbutton" type="submit">
              Know More
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
