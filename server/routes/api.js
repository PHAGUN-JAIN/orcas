require("dotenv").config();
const router = require("express").Router();
const Razorpay = require("razorpay");
const shortid = require("shortid");
const mongoose = require("mongoose");
const participant = mongoose.model("registration");
const mailer = require("../mailer");

/*
instantiating razorpay
*/
var instance = new Razorpay({
  key_id: process.env.KEY_ID,
  key_secret: process.env.KEY_SECRET,
});

/*
payments routes
*/
router.get("/payments", (req, res) => {
  res.json({ message: "hello from server /payments route" });
  // res.send("hello");
  res.end();
});

router.post("/payments/order", async (req, res) => {
  console.log("create orderid request to razor pay", req.body);

  /**
   * calculating discount
   */
  var newAmt;
  const all_event_check = req.body.event;
  const coupons = ["WAIVEIT", "PR"];

  if (all_event_check == "all3" && req.body.code == "ALLABOARD") {
    newAmt = 60;
  } else if (all_event_check == "all3" && req.body.code == "WAIVEIT") {
    newAmt = 120 - 20;
  } else if (all_event_check == "all3") {
    newAmt = 120;
  } else if (coupons.includes(req.body.code)) {
    newAmt = req.body.amount - 20;
  } else {
    newAmt = req.body.amount;
  }
  /*
  add user to db
  */

  participant.find({ email: req.body.email }, async (err, verify) => {
    // console.log(verify);

    const reg = new participant({
      name: req.body.name,
      email: req.body.email,
      phno: req.body.phno,
      cuponcode: req.body.code,
      event: req.body.event,
      webinar: req.body.webinar,
      amount: newAmt,
    });

    if (verify.length === 0) {
      console.log("did not existed");
      try {
        const p = await reg.save();
        // res.json(p);
        console.log("mongo res(server log)", p);
      } catch (error) {
        console.log("err while inserting" + error);
      }

      var options = {
        amount: newAmt * 100, // amount in the smallest currency unit
        currency: "INR",
        receipt: shortid.generate(),
        payment_capture: 1,
      };
      //checking for  events

      if (all_event_check.length !== 0) {
        console.log("length is not 0");
        await instance.orders.create(options, (err, order) => {
          if (err) {
            console.log(err);
          } else {
            console.log("server log", order);
            console.log(order.id);
            res.send({
              orderId: order.id,
              amount: newAmt,
              email: req.body.email,
            });
          }
        });
      }
      //sending email on registering for only webinar
      if (req.body.webinar.length !== 0) {
        mailer.send(req.body.email);
      }
    } else {
      // res.send(JSON.stringify(verify));
      console.log("email alredy registered");
      res.json({
        status:
          "email alredy registered! contact TeamUCD for modifying the registration",
      });
    }
  });

  // var options = {
  //   amount: req.body.amount * 100, // amount in the smallest currency unit
  //   currency: "INR",
  //   receipt: shortid.generate(),
  //   payment_capture: 1,
  // };

  // await instance.orders.create(options, (err, order) => {
  //   if (err) {
  //     console.log(err);
  //   } else {
  //     console.log(order);
  //     console.log(order.id);
  //     res.send({ orderId: order.id, amount: req.body.amount });
  //   }
  // });
});

router.post("/payments/verify", (req, res) => {
  console.log("server log", req.body);

  let body =
    req.body.data.razorpay_order_id + "|" + req.body.data.razorpay_payment_id;
  var crypto = require("crypto");
  var expectedSignature = crypto
    .createHmac("sha256", process.env.KEY_SECRET)
    .update(body.toString())
    .digest("hex");

  console.log("sig received ", req.body.data.razorpay_signature);

  console.log("sig generated ", expectedSignature);

  var response = { signatureIsValid: "false" };

  if (expectedSignature === req.body.data.razorpay_signature)
    response = { signatureIsValid: "true" };

  console.log("server log", response);
  // console.log(response.signatureIsValid);

  if (response.signatureIsValid === "true") {
    /*
     update payment status and send email
    */

    participant.updateOne(
      { email: req.body.email },
      { $set: { payment_status: "paid" } },
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log("server log", result);
        }
      }
    );
    mailer.send(req.body.email);
  }

  res.json(response);
});

/*
mongodb routes
*/

router.route("/register").get((req, res) => {
  // res.send("hello register");
  participant.find((err, docs) => {
    if (err) {
      console.log("api err");
    } else {
      console.log(docs);
      res.json(docs);
      res.end();
    }
  });
});

router.route("/register").post(async (req, res) => {
  console.log(req.body);

  participant.find({ email: req.body.email }, async (err, verify) => {
    // console.log(verify);

    if (verify.length === 0) {
      console.log("did not existed");
      // res.send("did not existed");
      const reg = new participant({
        name: req.body.name,
        email: req.body.email,
        phno: req.body.phno,
        cuponcode: req.body.code,
      });
      try {
        const p = await reg.save();
        res.json(p);
      } catch (error) {
        console.log("err while inserting" + error);
      }
    } else {
      // res.send(JSON.stringify(verify));
      res.send("alredy exist!");
    }
  });
});

module.exports = router;
