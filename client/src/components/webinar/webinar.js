import React from "react";
import "./webinar.css";
import numone from "../../assets/images/numone.svg";
import numtwo from "../../assets/images/numtwo.svg";
import numthree from "../../assets/images/numthree.svg";

export default function Webinar() {
  return (
    <>
      <div className="webinar">
        <div className="heading">
          <h1 className="head">Webinars</h1>
          <br />
          <p className="text">
            Hello fgyuejhruijeh werhyrtijehajshf eghreuegyhurh gfutfyserughfuij
            sfseueytrfuehfs fuefugsdhfhgsssf Hello fgyuejhruijeh
            werhyrtijehajshf eghreuegyhurh gfutfyserughfuij sfseueytrfuehfs
            fuefugsdhfhgsssf Hello fgyuejhruijeh werhyrtijehajshf eghreuegyhurh
            gfutfyserughfuij sfseueytrfuehfs fuefugsdhfhgsssf
          </p>
          <br />
        </div>
        <div className="webinar-card-wrapper">
          <div className="card1">
            <img className="imgone" src={numone} alt="one" />
            <br />
            <p2>
              All About Arificial
              <br /> Intelligence
            </p2>
            <br />
            <br />
            <p3>
              Artificial intelligence and <br /> Machine learning are two very
              <br /> hot buzzwords right now. ML
              <br /> is a type of artificial
              <br /> intelligence (AI) that allows
              <br /> software applications to
              <br /> become more accurate at
              <br /> predicting outcomes without
              <br /> being explicitly programmed
              <br /> to do so.
            </p3>
            <br />
            <br />
            <br />
            <a href="google.com">Read More</a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
          <div className="card2">
            <img className="imgone" src={numtwo} alt="two" />
            <br />
            <p2>
              Intro <br /> to Linux
            </p2>
            <br />
            <br />
            <p3>
              A webinar organized to give
              <br /> students an introduction to
              <br /> Linux, whichis an open source
              <br /> operating system that
              <br /> everyone should be familiar
              <br /> with. The speaker will go
              <br /> through the fundamentals of
              <br /> the Linux operating system
              <br /> and the platforms on which it
              <br /> may be used during the
              <br /> presentation.
            </p3>
            <br />
            <br />
            <a href="google.com">Read More</a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
          <div className="card3">
            <img className="imgone" src={numthree} alt="three" />
            <br />
            <p2>
              Understandin The <br /> Basics of DevOps
            </p2>
            <br />
            <br />

            <p3>
              DevOps influences the
              <br /> application lifecycle
              <br /> throughout its plan, develop,
              <br /> deliver, and operate phases.
              <br /> Each phase relies on the
              <br /> others, and the phases are
              <br /> not role-specific.
            </p3>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <a href="google.com">Read More</a>
            <br />
            <br />
            <button className="Register1" type="submit">
              Register Now
            </button>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
    </>
  );
}
