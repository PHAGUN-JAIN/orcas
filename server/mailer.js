require("dotenv").config();
const nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");

var transporter = nodemailer.createTransport(
  smtpTransport({
    host: "smtp.hostinger.in",
    secure: true,
    auth: {
      user: process.env.EMAIL_AUTHOR,
      pass: process.env.EMAIL_AUTHOR_SECRET,
    },
  })
);

function send(receiver) {
  const emailOptions = {
    from: process.env.EMAIL_AUTHOR,
    to: receiver,
    subject: "[ORCAS] You are Registered",
    text: "Thankyou for registering!",
    html: `<!doctype html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
    <title></title>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
    #outlook a{padding:0;}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table,td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}p{display:block;margin:0;}
    </style>
    <!--[if mso]> <noscript><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml></noscript>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
    .ogf{width:100% !important;}
    </style>
    <![endif]-->
    <style type="text/css">
    @media only screen and (min-width:599px){.pc100{width:100%!important;max-width:100%;}.pc49{width:49%!important;max-width:49%;}.pc2{width:2%!important;max-width:2%;}.xc600{width:600px!important;max-width:600px;}.xc536{width:536px!important;max-width:536px;}.xc552{width:552px!important;max-width:552px;}}
    </style>
    <style media="screen and (min-width:599px)">.moz-text-html .pc100{width:100%!important;max-width:100%;}.moz-text-html .pc49{width:49%!important;max-width:49%;}.moz-text-html .pc2{width:2%!important;max-width:2%;}.moz-text-html .xc600{width:600px!important;max-width:600px;}.moz-text-html .xc536{width:536px!important;max-width:536px;}.moz-text-html .xc552{width:552px!important;max-width:552px;}
    </style>
    <style type="text/css">
    @media only screen and (max-width:599px){table.fwm{width:100%!important;}td.fwm{width:auto!important;}}
    </style>
    <style type="text/css">
    u+#body a,#MessageViewBody a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important;}u+.body .glist{margin-left:0!important;}
    @media only screen and (max-width:599px){#body{height:100%!important;margin:0!important;padding:0!important;width:100%!important;}u+.body .glist{margin-left:25px!important;}td.x{padding-left:0!important;padding-right:0!important;}br.sb{display:none!important;}.hd-1{display:block!important;height:auto!important;overflow:visible!important;}div.r.pr-16>table>tbody>tr>td{padding-right:16px!important}div.r.pl-16>table>tbody>tr>td{padding-left:16px!important}div.r.pt-0>table>tbody>tr>td{padding-top:0px!important}div.r.pr-0>table>tbody>tr>td{padding-right:0px!important}div.r.pb-0>table>tbody>tr>td{padding-bottom:0px!important}div.r.pl-0>table>tbody>tr>td{padding-left:0px!important}td.b.fw-1>table{width:100%!important}td.fw-1>table>tbody>tr>td>a{display:block!important;width:100%!important;padding-left:0!important;padding-right:0!important;}td.b.fw-1>table{width:100%!important}td.fw-1>table>tbody>tr>td{width:100%!important;padding-left:0!important;padding-right:0!important;}}
    </style>
    <meta name="color-scheme" content="light dark">
    <meta name="supported-color-schemes" content="light dark">
    <!--[if gte mso 9]>
    <style>li{text-indent:-1em;}
    </style>
    <![endif]-->
    </head>
    <body id="body" style="word-spacing:normal;background-color:#232323;"><div style="background-color:#232323;">
    <!--[if mso | IE]>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="r-outlook -outlook pr-16-outlook pl-16-outlook -outlook" style="width:600px;" width="600" bgcolor="#1b1b1b"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="r pr-16 pl-16 " style="background:#1b1b1b;background-color:#1b1b1b;margin:0px auto;border-radius:0;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#1b1b1b;background-color:#1b1b1b;width:100%;border-radius:0;"><tbody><tr><td style="border:none;direction:ltr;font-size:0;padding:16px 16px 16px 16px;text-align:left;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:568px;">
    <![endif]--><div class="pc100 ogf" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:middle;width:278px;">
    <![endif]--><div class="pc49 ogf m c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td align="left" class="x " style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;"><div style="font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:16px;line-height:20px;text-align:left;color:#000000;"><p style="Margin:0;"><span style="mso-line-height-rule:exactly;font-size:16px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:700;color:#d8d8d8;line-height:20px;">BGMI Tournament</span></p></div>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td><td style="vertical-align:top;width:11px;">
    <![endif]--><div class="pc2 ogf g" style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:2%;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="vertical-align:top;padding:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="" width="100%"><tbody></tbody></table>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td><td style="vertical-align:middle;width:278px;">
    <![endif]--><div class="pc49 ogf c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td class="" style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;">
    <!--[if mso | IE]>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:278.32px;" width="278"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="" style="margin:0px auto;max-width:278.32px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;"><tbody><tr><td style="direction:ltr;font-size:0;padding:0;padding-bottom:0;text-align:center;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:278px;">
    <![endif]--><div class="pc100 ogf" style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="vertical-align:top;padding:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="" width="100%"><tbody><tr><td align="right" class="c " style="font-size:0;padding:0;word-break:break-word;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;" class="fwm"><tbody><tr><td style="width:86px;" class="fwm"> <img alt="" height="auto" src="https://ucdupes.org/img/mail-asset/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="86">
    </td></tr></tbody></table>
    </td></tr></tbody></table>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]--></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="r-outlook -outlook pt-0-outlook pr-0-outlook pb-0-outlook pl-0-outlook -outlook" style="width:600px;" width="600" bgcolor="#ffffff"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="r pt-0 pr-0 pb-0 pl-0 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0;"><tbody><tr><td style="border:none;direction:ltr;font-size:0;padding:0;text-align:left;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="c-outlook -outlook -outlook" style="vertical-align:middle;width:600px;">
    <![endif]--><div class="xc600 ogf c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td align="center" class="i fw-1 " style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;" class="fwm"><tbody><tr><td style="width:600px;" class="fwm"> <img alt="" height="auto" src="https://ucdupes.org/img/mail-asset/bgmi.jpg" style="border:0;border-radius:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600">
    </td></tr></tbody></table>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="r-outlook -outlook pr-16-outlook pl-16-outlook -outlook" style="width:600px;" width="600" bgcolor="#1b1b1b"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="r pr-16 pl-16 " style="background:#1b1b1b;background-color:#1b1b1b;margin:0px auto;border-radius:0;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#1b1b1b;background-color:#1b1b1b;width:100%;border-radius:0;"><tbody><tr><td style="border:none;direction:ltr;font-size:0;padding:32px 32px 32px 32px;text-align:left;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="c-outlook -outlook -outlook" style="vertical-align:middle;width:536px;">
    <![endif]--><div class="xc536 ogf c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td align="left" class="x m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;"><div style="font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:28px;line-height:32px;text-align:left;color:#000000;"><p style="Margin:0;"><span style="mso-line-height-rule:exactly;font-size:28px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:700;color:#d8d8d8;line-height:32px;">Hello Captain</span></p></div>
    </td></tr><tr><td align="left" class="x " style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;"><div style="font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:16px;line-height:24px;text-align:left;color:#000000;"><p style="Margin:0;"><span style="mso-line-height-rule:exactly;font-size:16px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:400;color:#d8d8d8;line-height:24px;">Thank you for registering in the Battlegrounds Mobile India Tournament by UCD. Starting from 17th of September to 19th of September between 6-8 pm ,this is a very thrilling opportunity for all the gamers and non gamers to shill their skills. </span></p></div>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="r-outlook -outlook pr-16-outlook pl-16-outlook -outlook" style="width:600px;" width="600" bgcolor="#1b1b1b"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="r pr-16 pl-16 " style="background:#1b1b1b;background-color:#1b1b1b;margin:0px auto;border-radius:0;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#1b1b1b;background-color:#1b1b1b;width:100%;border-radius:0;"><tbody><tr><td style="border:none;direction:ltr;font-size:0;padding:24px 24px 24px 24px;text-align:left;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="c-outlook -outlook -outlook" style="vertical-align:middle;width:552px;">
    <![endif]--><div class="xc552 ogf c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td align="center" class="i fw-1 m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;" class="fwm"><tbody><tr><td style="width:552px;" class="fwm"> <img alt="" height="auto" src="https://ucdupes.org/img/mail-asset/whatsapp.jpg" style="border:0;border-radius:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="552">
    </td></tr></tbody></table>
    </td></tr><tr><td class="s m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;"><div style="height:16px;line-height:16px;">&#8202;</div>
    </td></tr><tr><td align="left" class="x m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;"><div style="font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:32px;line-height:37px;text-align:left;color:#000000;"><p style="Margin:0;"><span style="mso-line-height-rule:exactly;font-size:32px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:700;color:#d8d8d8;line-height:37px;">Communication is Key</span></p></div>
    </td></tr><tr><td align="left" class="x m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;"><div style="font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:16px;line-height:22px;text-align:left;color:#000000;"><p style="Margin:0;"><span style="mso-line-height-rule:exactly;font-size:16px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:400;color:#d8d8d8;line-height:22px;">For further updates and smoother communication join this whatsapp group. Our teams will be in touch with you constantly and help you out with any hindances.</span></p></div>
    </td></tr><tr><td class="s m" style="font-size:0;padding:0;padding-bottom:8px;word-break:break-word;"><div style="height:4px;line-height:4px;">&#8202;</div>
    </td></tr><tr><td align="center" vertical-align="middle" class="b fw-1 " style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;width:118px;line-height:100%;"><tbody><tr><td align="center" bgcolor="#eeeeee" style="border:none;border-radius:60px 60px 60px 60px;cursor:auto;mso-padding-alt:12px 0px 12px 0px;background:#eeeeee;" valign="middle"> <a href="https://chat.whatsapp.com/HwgFtUpizGMA8MQ0Nw9ksm" style="display:inline-block;width:118px;background:#eeeeee;color:#ffffff;font-family:Nimbus Sans Extd,Arial,sans-serif;font-size:13px;font-weight:normal;line-height:100%;margin:0;text-decoration:none;text-transform:none;padding:12px 0px 12px 0px;mso-padding-alt:0;border-radius:60px 60px 60px 60px;" target="_blank"> <span style="mso-line-height-rule:exactly;font-size:14px;font-family:Nimbus Sans Extd,Arial,sans-serif;font-weight:700;color:#1b1b1b;line-height:16px;">Join Now</span></a>
    </td></tr></tbody></table>
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="r-outlook -outlook pr-16-outlook pl-16-outlook -outlook" style="width:600px;" width="600" bgcolor="#1b1b1b"><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]--><div class="r pr-16 pl-16 " style="background:#1b1b1b;background-color:#1b1b1b;margin:0px auto;border-radius:0;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#1b1b1b;background-color:#1b1b1b;width:100%;border-radius:0;"><tbody><tr><td style="border:none;direction:ltr;font-size:0;padding:16px 16px 16px 16px;text-align:left;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:568px;">
    <![endif]--><div class="pc100 ogf" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
    <!--[if mso | IE]>
    <table border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:middle;width:568px;">
    <![endif]--><div class="pc100 ogf c " style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:transparent;border:none;vertical-align:middle;" width="100%"><tbody><tr><td align="right" class="o " style="font-size:0;padding:0;padding-bottom:0;word-break:break-word;">
    <!--[if mso | IE]>
    <table align="right" border="0" cellpadding="0" cellspacing="0"><tr><td>
    <![endif]-->
    <table align="right" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;"><tbody><tr class="e m"><td style="padding:0 16px 0 0;vertical-align:top;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-radius:0;width:20px;"><tbody><tr><td style="font-size:0;height:20px;vertical-align:middle;width:20px;"> <a href="#insertUrlLink" target="_blank"> <img alt="Instagram" height="20" src="https://ucdupes.org/img/mail-asset/insta.png" style="border-radius:0;display:block;" width="20"></a>
    </td></tr></tbody></table>
    </td></tr></tbody></table>
    <!--[if mso | IE]>
    </td><td>
    <![endif]-->
    <table align="right" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;"><tbody><tr class="e m"><td style="padding:0 16px 0 0;vertical-align:top;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-radius:0;width:20px;"><tbody><tr><td style="font-size:0;height:20px;vertical-align:middle;width:20px;"> <a href="#insertUrlLink" target="_blank"> <img alt="Facebook" height="20" src="https://ucdupes.org/img/mail-asset/fb.png" style="border-radius:0;display:block;" width="20"></a>
    </td></tr></tbody></table>
    </td></tr></tbody></table>
    <!--[if mso | IE]>
    </td><td>
    <![endif]-->
    <table align="right" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;"><tbody><tr class="e m"><td style="padding:0 16px 0 0;vertical-align:top;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-radius:0;width:20px;"><tbody><tr><td style="font-size:0;height:20px;vertical-align:middle;width:20px;"> <a href="#insertUrlLink" target="_blank"> <img alt="Twitter" height="20" src="https://ucdupes.org/img/mail-asset/tw.png" style="border-radius:0;display:block;" width="20"></a>
    </td></tr></tbody></table>
    </td></tr></tbody></table>
    <!--[if mso | IE]>
    </td><td>
    <![endif]-->
    <table align="right" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;"><tbody><tr class="e "><td style="padding:0;vertical-align:top;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-radius:0;width:20px;"><tbody><tr><td style="font-size:0;height:20px;vertical-align:middle;width:20px;"> <a href="#insertUrlLink" target="_blank"> <img alt="YouTube" height="20" src="https://ucdupes.org/img/mail-asset/yt.png" style="border-radius:0;display:block;" width="20"></a>
    </td></tr></tbody></table>
    </td></tr></tbody></table>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]--></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]-->
    </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]--></div>
    </body>
    </html>`,
  };

  console.log(receiver);

  transporter.sendMail(emailOptions, (err, info) => {
    if (err) {
      console.error(err);
    }
    console.log("email sent:" + info.response);
  });
}

module.exports = { send };
