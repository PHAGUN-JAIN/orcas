const mongoose = require("mongoose");

const participantSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  phno: { type: Number },
  cuponcode: { type: String },
  amount: { type: Number },
  event: { type: String },
  webinar: { type: String },
  payment_status: { type: String, default: "pending" },
});

module.exports = mongoose.model("registration", participantSchema);
