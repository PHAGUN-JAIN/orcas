import staytuned from "../../assets/images/staytuned.png";
import bgImg from "../../assets/images/bottm-banner.svg";
export default function Bottom() {
  return (
    <>
      <div style={{ position: "relative" }}>
        <img src={bgImg} alt="bottom-banner" />
        <img
          src={staytuned}
          alt="staytuned"
          style={{
            width: "20%",
            height: "auto",
            position: "absolute",
            left: "40%",
            bottom: "20%",
          }}
        />
      </div>
    </>
  );
}
