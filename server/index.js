require("dotenv").config();
require("./models");
const express = require("express");
const app = express();
const path = require("path");
const cors = require("cors");
const PORT = process.env.PORT || 3001;
const router = require("./routes");

app.use(express.static(path.resolve(__dirname, "../client/build")));
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json()); //server does not understand json thats why a middleware
app.use(cors());
app.get("/api", router);
// app.get("/api/register", router);
// app.post("/api/register", router);
app.get("/api/payments", router);
app.post("/api/payments/order", router);
app.post("/api/payments/verify", router);

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../client/build", "index.html"));
});

app.listen(PORT, () => {
  console.log(`Listening to port: ${PORT}`);
});
