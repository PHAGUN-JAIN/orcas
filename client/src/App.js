import React from "react";
import "./index.css";
import { useFormik } from "formik";
import { BsFillPersonFill, BsFillCalendarEventFill } from "react-icons/bs";
import { MdEmail } from "react-icons/md";
import { FaPhoneAlt, FaMicrophone } from "react-icons/fa";
import { RiCoupon3Fill } from "react-icons/ri";
import Navbar from "./components/navbar/Navbar";
import Hero from "./components/hero/Hero";
import Bottom from "./components/bottom/Bottom";
import Footer from "./components/footer/Footer";
import Webinar from "./components/webinar/webinar";
import Events from "./components/events/events";

// require("./setupProxy.js");

function loadScript(src) {
  return new Promise((resolve) => {
    const script = document.createElement("script");
    script.src = src;
    script.onload = () => {
      resolve(true);
    };
    script.onerror = () => {
      resolve(false);
    };
    document.body.appendChild(script);
  });
}

async function verifyPay(response, Remail) {
  return await fetch("http://localhost:3001/api/payments/verify", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      email: Remail,
      data: response,
    }),
  }).then((t) => t.json());
}

function App() {
  async function displayRazorpay(value) {
    const res = await loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }

    //checking for events registration
    console.log(value);
    if (value.event.length === 0 && value.webinar.length !== 0) {
      alert("ThankYou for registering!!");
    }

    //creating orderid for processing checkout
    const data = await fetch("/api/payments/order", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(value),
    }).then((t) => t.json());

    console.log(data);
    if (
      data.status !==
      "email alredy registered! contact TeamUCD for modifying the registration"
    ) {
      const options = {
        key: process.env.RAZORPAY_KEY_ID,
        currency: "INR",
        amount: data.amount,
        name: "United Club of Developers",
        description: "ORCAS-2021 UCD",
        image: "https://ucdupes.org/img/logo.png",
        order_id: data.orderId,
        handler: (response) => {
          console.log(response);
          //here send the data to server for storing
          const verification = verifyPay(response, data.email);
          console.log(verification);
          verification.then((val) => console.log(val.signatureIsValid));

          alert(
            "Thanks for registering please take note of this payment id: " +
              response.razorpay_payment_id
          );
          // alert(response.razorpay_order_id);
          // alert(response.razorpay_signature);
          console.log(response);
        },
        theme: {
          color: "#99cc81",
        },
      };

      const paymentObject = new window.Razorpay(options);
      paymentObject.open();
    } else {
      alert(data.status);
    }
  }

  function Form() {
    const formik = useFormik({
      initialValues: {
        name: "",
        email: "",
        phno: "",
        code: "",
        amount: "50",
        event: "",
        webinar: "",
      },
      onSubmit: (value) => {
        console.log(value);
        displayRazorpay(value);
      },
    });

    return (
      <div id="abc" className="form-wrapper">
        <div className="heading-container">
          <h1>
            <RiCoupon3Fill /> Event Registrations
          </h1>
        </div>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <div className="inner-wrapper">
              <div className="form-p1">
                <label>
                  <BsFillPersonFill /> &nbsp; Name
                </label>
                <br />
                <input
                  typeof="text"
                  id="name"
                  name="name"
                  placeholder="fullname"
                  required
                  onChange={formik.handleChange}
                  value={formik.values.name}
                ></input>
                <br />
                <br />
                <label>
                  <MdEmail /> &nbsp; E-Mail
                </label>
                <br />
                <input
                  typeof="email"
                  name="email"
                  id="pid"
                  placeholder="email"
                  required
                  onChange={formik.handleChange}
                  value={formik.values.email}
                ></input>
                <br />
                <br />
                <label>
                  <FaPhoneAlt /> &nbsp; Contact Number
                </label>
                <br />
                <input
                  typeof="Number"
                  id="phno"
                  name="phno"
                  placeholder="contact no"
                  required
                  onChange={formik.handleChange}
                  value={formik.values.phno}
                ></input>
                <br />
              </div>
              <div className="form-p2">
                <label>
                  <BsFillCalendarEventFill /> &nbsp; Select Events
                </label>
                <br />
                <select
                  id="event"
                  name="event"
                  onChange={formik.handleChange}
                  value={formik.values.event}
                >
                  <option default value="">
                    Select event
                  </option>
                  <option value="battlecode">BattleCode</option>
                  <option value="efficaciouscoding">
                    Efficacious Coding Challenge
                  </option>
                  <option value="shuffledefiance">Shuffle Defiance</option>
                  <option value="all3">All</option>
                </select>
                <br />
                <br />
                <label>
                  <FaMicrophone /> &nbsp; Select Webinars
                </label>
                <br />
                <select
                  id="webinar"
                  name="webinar"
                  onChange={formik.handleChange}
                  value={formik.values.webinar}
                >
                  <option default value="">
                    Select webinar
                  </option>
                  <option value="allaboutai">
                    All About Artificial Intelligence
                  </option>
                  <option value="introlinux">Intro To Linux</option>
                  <option value="devops">
                    Understanding The Basics Of Devops
                  </option>
                  <option value="all3">All</option>
                </select>
                <br />
                <br />
                <label>
                  <RiCoupon3Fill /> &nbsp; Coupon Code{" "}
                </label>
                <br />
                <input
                  typeof="text"
                  name="code"
                  id="code"
                  placeholder="coupon code(if any)"
                  onChange={formik.handleChange}
                  value={formik.values.code}
                ></input>
                <br />
                <br />
                <input type="checkbox" id="TC" name="TC" required></input>
                <label>
                  {" "}
                  I have read and accept the &nbsp;
                  <a
                    id="terms"
                    href="https://ucdupes.org/terms-conditions/payment-policy.html"
                  >
                    terms & conditions
                  </a>
                </label>
                <br />
                <br />
                <button type="submit">Sign Me Up!</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
  return (
    <>
      <Navbar />
      <Hero />
      <Form />
      <Events />
      <Webinar />
      <Bottom />
      <Footer />
    </>
  );
}

export default App;
