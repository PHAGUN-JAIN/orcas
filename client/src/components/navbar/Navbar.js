import "./Navbar.css";
import orcas from "../../assets/images/orcasisback.svg";
export default function Navbar() {
  return (
    <>
      <div className="wrapper">
        <img src={orcas} alt="orcas is back" className="orcas-branding" />
        <div className="right">
          <ul>
            <li>
              <a href="https://ucdupes.org/Aboutus.html">About</a>
            </li>
            <li>
              <a href="https://ucdupes.org">Events</a>
            </li>
            <li>
              <a href="https://ucdupes.org" className="register">
                RegisterNow
              </a>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
